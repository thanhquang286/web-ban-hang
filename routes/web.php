<?php

use App\Http\Controllers\Admin\AdminNewsController;
use Illuminate\Support\Facades\Route;
use App\Models\News;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/news', function(){
//     return view('news');
// });
// Route::prefix('name')->group(function(){
//     Route::get('/home',[NameController::class, 'index']);
//     Route::get('/create', [NameController::class, 'create']);
//     Route::post('/create', [NameController::class, 'store']);
    
// });
Route::get('/admin/news/create',[AdminNewsController::class, 'create']);
Route::post('/admin/news/store',[AdminNewsController::class, 'store']);
Route::view('/admin/login','admin.login');